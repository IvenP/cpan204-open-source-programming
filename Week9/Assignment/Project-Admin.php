<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Admin Page</title>
</head>
<body>
<form method="post">
    <h2>Hello, Admin</h2>
    <form >
        <button type="submit" name="users">View All User Accounts</button>

        <button type="submit" name="create">Create User Account</button>

        <button type="submit" name="reports" >View Problem Reports</button>
    </form>

</form>
</body>
</html>

<?php
if(isset($_POST['users'])) {
    header('location:Project-UserAccounts.php');
}
elseif (isset($_POST['create'])){
    header('location:Project-CreateAccountEMP.php');
}
elseif (isset($_POST['reports'])){
    header('location:Project-ViewReports.php');
}