<?php
// declaring some variables
$host = "localhost";
$user = "root";
$password = "";
$dbName = "project-xyz";
$Email = '';
$Des = '';
$Level='';
$Location = '';
$Id = '';

//Connect to the Server+Select DB
$con = mysqli_connect($host, $user, $password, $dbName)
or die("Connection is failed");


//Update & Edit
if(isset($_POST['UPDATE'])){
    $Email = $_POST['email'];
    $Des = $_POST['desc'];
    $Level= $_POST['level'];
    $Location = $_POST['location'];
    $Id = $_POST['id'];

    if(!empty($_POST['email'])) {
        $query = "Update report Set email ='$Email', description = '$Des', level = '$Level', location = '$Location', id = '$Id' 
        where email = '$Email'";

        $result = mysqli_query($con, $query) or die ("query is failed" . mysqli_error($con));
        $Email = '';
        $Des = '';
        $Level='';
        $Location = '';
        $Id = '';

        echo " <h3>Data is just successfully updated</h3>";
    }else{
        echo "Update is failed. Fill all the information to update.";
    }
}
//Delete
if(isset($_POST['DELETE'])){
    $Email = $_POST['email'];

    //Step 3 - 3
    if(!empty($_POST['email'])){
        $query = "Delete from report where email = '$Email'";
        echo " You successfully delete ' ".$Email." ' from the table";

        $result = mysqli_query($con, $query) or die ("query is failed" . mysqli_error($con));
        $Email = '';
        $Des = '';
        $Level='';
        $Location = '';
        $Id = '';
    }else{
        echo "Fail to delete. Try again.";
    }
}
//Find
if(isset($_POST['FIND'])){
    $Email = $_POST['email'];

    $query = "Select * from report where email = '$Email'";
    $result = mysqli_query($con, $query) or die ("query is failed" . mysqli_error($con));
    if (($row = mysqli_fetch_row($result)) == true) {

        $Email = $row[0];
        $Des = $row[1];
        $Level=$row[2];
        $Location = $row[3];
        $Id = $row[4];
    }
    else echo "Record not found";
}

//Retrieve All
$query = "Select * from report";
$result = mysqli_query($con, $query) or die ("query is failed" . mysqli_error($con));

mysqli_close($con);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" type="text/css" href="Project-basic.css">
    <meta charset="UTF-8">
    <title>Search Form</title>
</head>
<body>
<form method="post">
    <span id="title">You login as Admin: View problem reports</span>
    <p> User Email: <input type="text" placeholder="Enter Email" name="email" value="<?php echo $Email ?>" /> </p>
    <p> Description: <input type="text" name="desc" value="<?php echo $Des ?>" /> </p>
    <p> Level of Urgency: <input type="text" name="level" value="<?php echo $Level ?>" />  </p>
    <p> Location: <input type="text" name="location" value="<?php echo $Location ?>" />  </p>
    <p> ID: <input type="text" name="id" value="<?php echo $Id ?>" />  </p>
    <input type="submit" value="Update" name="UPDATE" />
    <input type="submit" value="Delete" name="DELETE" />
    <input type="submit" value="Find" name="FIND" />
</form>

    <h3 id="second_title"> - View problem reports - </h3>
    <?php
    echo "<table  border='1' >";
    echo "<tr><th>Email</th><th>Description</th><th>Level of Urgency</th><th>Location</th><th>ID</th><th>Date</th></tr>";
    while (($row = mysqli_fetch_row($result)) == true) {
    echo "<tr><td>$row[0]</td><td>$row[1]</td><td>$row[2]</td><td>$row[3]</td><td>$row[4]</td><td>$row[5]</td></tr>";
}
    echo "</table>";
?>
</body>
</html>