-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 05, 2018 at 03:40 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project-xyz`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(3) NOT NULL,
  `issue` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `issue`) VALUES
(1, 'Internet-disconnects'),
(2, 'Login-failed'),
(3, 'Wifi-Error'),
(4, 'Internet-slow'),
(5, 'login-forgot'),
(6, 'login-forgotemail');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `eName` varchar(20) NOT NULL,
  `ePass` varchar(20) NOT NULL,
  `eCell` int(10) NOT NULL,
  `eEmail` varchar(25) NOT NULL,
  `eAddress` varchar(30) NOT NULL,
  `emp_id` int(3) NOT NULL,
  `admin` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`eName`, `ePass`, `eCell`, `eEmail`, `eAddress`, `emp_id`, `admin`) VALUES
('dave', '123', 55555533, 'dave@123', 'barrie', 1, 1),
('bob', '1234', 123456, 'bob.com', 'toronto', 4, 0),
('joe', '123', 1234567, 'joe.com', 'brampton', 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `guest`
--

CREATE TABLE `guest` (
  `gName` varchar(20) NOT NULL,
  `gPass` varchar(20) NOT NULL,
  `gCell` int(10) NOT NULL,
  `gEmail` varchar(25) NOT NULL,
  `gAddress` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guest`
--

INSERT INTO `guest` (`gName`, `gPass`, `gCell`, `gEmail`, `gAddress`) VALUES
('iven', '12345', 12345676, 'iven@1234.com', 'brampton');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `email` varchar(25) NOT NULL,
  `description` varchar(255) NOT NULL,
  `level` int(1) NOT NULL,
  `location` varchar(30) NOT NULL,
  `id` varchar(15) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`email`, `description`, `level`, `location`, `id`, `date`) VALUES
('bob.com', 'Does not work', 5, 'Toronto', 'bob', '2018-11-04'),
('bob.com', 'forgot account details', 2, 'brampton', 'bob', '2018-11-04'),
('bob.com', 'wifi is slow', 4, 'barrie', 'bob', '2018-11-04'),
('bob.com', '', 1, '', 'bob', '2018-11-04');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `guest`
--
ALTER TABLE `guest`
  ADD PRIMARY KEY (`gEmail`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
