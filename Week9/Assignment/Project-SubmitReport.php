<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Assignment</title>
</head>
<body>
<form method="post">
    <h1>Submit Incident Report:</h1>
     Incident:

    <?php
//php code to fill select drop box from category table

    $host = "localhost";
    $user = "root";
    $password = "";
    $dbName = "project-xyz";


    $con = mysqli_connect($host,$user,$password,$dbName) or die("Connection failed");


    $query2 = "SELECT issue FROM category";
    $result2 = mysqli_query($con, $query2) or die ("Query failed" . mysqli_error($con));
    //adds row to the option
    echo "<select name='issue'>";
    while ($row = mysqli_fetch_array($result2)) {
        echo "<option value='" . $row['issue'] . "'>" . $row['issue'] . "</option>";
    }
    echo "</select>";



    mysqli_close($con);

    ?>
    <br><br>Description:<br>
    <textarea name="desc"></textarea>
    <br><br>Level
    <select name="level">
        <option>1</option>
        <option>2</option>
        <option>3</option>
        <option>4</option>
        <option>5</option>
    </select>
    <br><br>Location
    <input type="text" name="location">
    <br><br>
    <input type="submit" value="Submit" name="submit"/>

</form>

</body>
</html>
<?php
session_start();
if(isset($_POST['submit'])) {

    //Declaring some variables to connect to DB
    $host = "localhost";
    $user = "root";
    $password = "";
    $dbName = "project-xyz";


    //Connect to the Server + Select DB
    $con = mysqli_connect($host, $user, $password, $dbName) or die("Connection is failed");

    //Formulate Query and Pose the Query
    $issue = $_POST['issue'];
    $desc = $_POST['desc'];
    $level = $_POST['level'];
    $location = $_POST['location'];
    //retrieve username from previous page
    $eUser = $_SESSION['eUser'];
    $gUser = $_SESSION['gUser'];
    //checks if user is employee or guest and then adds to that specific table
    if(!empty($eUser)) {
        $email = mysqli_query($con,"select * from employee where eName = '$eUser'");
        //retrieves the email from username from the table
        $row = mysqli_fetch_assoc($email);
        $mail = $row['eEmail'];
        $query = "Insert into report values ('$mail','$desc','$level','$location','$eUser',curdate())";

    }
    else if(!empty($gUser)) {
        $email = mysqli_query($con,"select * from guest where gName = '$gUser'");
        $row = mysqli_fetch_assoc($email);
        $mail = $row['gEmail'];
        $query = "Insert into report values ('$mail','$desc','$level','$location','$gUser',curdate())";


    }
    function Alert($msg) {
        echo '<script type="text/javascript">alert("' . $msg . '")</script>';
    }

    Alert("Report Successfully submitted");
//open a write to a log file to store username and date
    $result = mysqli_query($con, $query) or die ("query is failed!" . mysqli_error($con));
    $myfile = fopen('log.txt','a');
    $mydata = $eUser.$gUser. " " .date( 'Y-m-d h:i:s A')."\n";

//write

    fwrite($myfile,$mydata);
//close

    fclose($myfile);

    //Close the connection
    mysqli_close($con);
}

