<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <title>Webform1</title>

</head>
<body>
<form method="get">
    <p>
        <label>Invoice No:</label>
        <input type="text" name="Invoice"/>
    </p>
    <p>
        <label for="select">Branch:</label>
        <select name="Sale">
            <option value="Toronto">Toronto</option>
            <option value="Windsor">Windsor</option>
            <option value="Brampton">Brampton</option>
        </select>
    </p>
    <input type="submit" value="Search"/>
</form>
</body>
</html>

<?php
/**
 * Created by PhpStorm.
 * User: iven_
 * Date: 2018-09-27
 * Time: 2:16 PM
 */
if(isset($_GET["Invoice"]) && isset($_GET["Sale"])) {

    $Invoice = $_GET["Invoice"];
    $Sale = $_GET["Sale"];
    echo "Please wait to complete the search process of invoice No: $Invoice in $Sale Branch";
}
?>